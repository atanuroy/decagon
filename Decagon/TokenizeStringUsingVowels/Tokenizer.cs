﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokenizeStringUsingVowels
{
	/// <summary>
	/// This class takes care of the Tokenization and printing the result
	/// </summary>
	class Tokenizer
	{
		/// <summary>
		/// Stores the tokens
		/// </summary>
		private List<string> _tokens = new List<string>();

		/// <summary>
		/// Stores the dilimiters (vowels)
		/// </summary>
		private readonly char[] _dilimiters = new [] {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};

		/// <summary>
		/// Tokenizes the input string
		/// </summary>
		/// <param name="input">Input string</param>
		internal void Tokenize(string input)
		{
			_tokens = input.Split(_dilimiters.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
		}

		/// <summary>
		/// Prints the result
		/// </summary>
		/// <returns>Result string</returns>
		internal string PrintResult()
		{
			string result = string.Empty;
			foreach (string token in _tokens)
			{
				result += " " + token;
			}
			return result.Trim().Replace(" ", ",");
		}
	}
}
