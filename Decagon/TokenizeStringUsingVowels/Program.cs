﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokenizeStringUsingVowels
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Welcome to Decagon Problem #2");
			Console.WriteLine();
			Console.WriteLine("Please enter the input string - ");
			Tokenizer t = new Tokenizer();
			t.Tokenize(Console.ReadLine());
			Console.WriteLine("The result is : " + t.PrintResult());
			Console.ReadLine();
		}
	}
}
