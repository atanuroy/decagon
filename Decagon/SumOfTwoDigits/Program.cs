﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SumOfTwoDigits
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Welcome to Decagon Problem #1");
			Console.WriteLine();
			Console.WriteLine("Please enter a two digit number - ");
			Calculator c = new Calculator();
			string input = Console.ReadLine();
			while (!c.ValidateInput(input))
			{
				Console.WriteLine(c.ValidationMessage);
				Console.WriteLine("Please enter the number again - ");
				input = Console.ReadLine();
			}
			Console.WriteLine("The sum is : " + c.CalculateSum());
			Console.ReadKey();
		}
	}
}
