﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumOfTwoDigits
{
	/// <summary>
	/// This class takes care of the validation and summation of the input integer
	/// </summary>
	class Calculator
	{
		/// <summary>
		/// Stores the validation message of input
		/// </summary>
		internal string ValidationMessage { get; private set; }

		/// <summary>
		/// Stores the input integer
		/// </summary>
		private int givenInteger;

		/// <summary>
		/// Validates if the input is correct
		/// </summary>
		/// <param name="input">User input</param>
		/// <returns>Boolean indicating if the input is valid or not</returns>
		internal bool ValidateInput(string input)
		{
			if (!int.TryParse(input, out givenInteger))
			{
				ValidationMessage = "This is not a valid integer.";
				return false;
			}
			if (givenInteger < 0)
			{
				ValidationMessage = "No negative number please.";
				return false;
			}
			if (givenInteger > 99 || givenInteger < 11)
			{
				ValidationMessage = "Only two digit numbers are accepted.";
				return false;
			}
			ValidationMessage = "Input is valid.";
			return true;
		}

		/// <summary>
		/// Adds the two digits and returns the result.
		/// </summary>
		/// <returns>The sum of the two digits</returns>
		internal string CalculateSum()
		{
			string givenIntegerAsString = givenInteger.ToString();
			IEnumerable<int> digits = givenIntegerAsString.Select(digit => int.Parse(digit.ToString()));
			int sum = 0;
			foreach (int digit in digits)
			{
				sum += digit;
			}
			return sum.ToString();
		}
	}
}
